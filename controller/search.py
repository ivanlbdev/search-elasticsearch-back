from service.search import Search


class SearchController:
    def __init__(self, body: dict, is_full: bool = False):
        self.params = body
        self.is_full = is_full

    def get(self):
        if self.is_full:
            return Search(self.params).get_full_search()
        else:
            return Search(self.params).get_presearch()
