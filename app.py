import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from controller.search import SearchController

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)


@app.get('/api/search/')
async def root(text, count, from_search=1, full=0):
    params = {
        'text': text,
        'count': int(count),
        'from': int(from_search)
    }
    is_full = lambda x, y: x if int(full) != 0 else y
    return SearchController(params, is_full(True, False)).get()


if __name__ == '__main__':
    uvicorn.run('app:app', reload=True)
