from starlette.testclient import TestClient

from app import app

client = TestClient(app)


def test_status():
    response = client.get('/api/search/?text=дрель&count=5')
    assert response.status_code == 200
