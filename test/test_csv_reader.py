from utils.csv_reader import CsvUtil


def test_csv_reader():
    csv_client = CsvUtil(base_path='test/')
    data_dict = {
        'test': [
            {'es_name': 'id', 'csv_name': 'id'},
            {'es_name': 'name', 'csv_name': 'name'},
        ]
    }

    csv_data = csv_client.csv_reader_to_dict('test', data_dict['test'])
    assert csv_data[0]['name'] == 'Ivan'
