from service.service import Service
from utils.csv_reader import CsvUtil
from elasticsearch7 import Elasticsearch


class ES(Service):
    def __init__(self):
        self.host = 'http://localhost:9200'
        self.es_client = Elasticsearch(self.host)

        self.index_products = 'example_items'
        self.index_categories = 'example_categories'
        self.index_brands = 'example_brands'

        self.settings = {
            'index': {
                'analysis': {
                    'filter': {
                        'stemmer_russian': {
                            'name': 'russian',
                            'type': 'stemmer'
                        },
                        'synonym': {
                            'type': 'synonym',
                            'synonyms_path': 'analysis/synonym.txt'
                        },
                        'latin_to_russian': {
                            'type': 'icu_transform',
                            'id': 'Cyrillic; NFD; [:Nonspacing Mark:] Remove; NFC'
                        },
                    },
                    'char_filter': {
                        'rus_char': {
                            'type': 'mapping',
                            'mappings': [
                                'ё => е',
                                'й => и'
                            ]
                        },
                        'rus_en_key': {
                            'type': 'mapping',
                            'mappings': [
                                'a => ф',
                                'b => и',
                                'c => с',
                                'd => в',
                                'e => у',
                                'f => а',
                                'g => п',
                                'h => р',
                                'i => ш',
                                'j => о',
                                'k => л',
                                'l => д',
                                'm => ь',
                                'n => т',
                                'o => щ',
                                'p => з',
                                'r => к',
                                's => ы',
                                't => е',
                                'u => г',
                                'v => м',
                                'w => ц',
                                'x => ч',
                                'y => н',
                                'z => я',
                                'A => Ф',
                                'B => И',
                                'C => С',
                                'D => В',
                                'E => У',
                                'F => А',
                                'G => П',
                                'H => Р',
                                'I => Ш',
                                'J => О',
                                'K => Л',
                                'L => Д',
                                'M => Ь',
                                'N => Т',
                                'O => Щ',
                                'P => З',
                                'R => К',
                                'S => Ы',
                                'T => Е',
                                'U => Г',
                                'V => М',
                                'W => Ц',
                                'X => Ч',
                                'Y => Н',
                                'Z => Я',
                                '[ => х',
                                '] => ъ',
                                '; => ж',
                                '< => б',
                                '> => ю'
                            ]
                        }
                    },
                    'analyzer': {
                        'default': {
                            'filter': [
                                'lowercase',
                                'synonym',
                                'stemmer_russian',
                                'unique'
                            ],
                            'char_filter': [
                                'rus_char'
                            ],
                            'type': 'custom',
                            'tokenizer': 'standard'
                        },
                        'rus_en_key_analyzer': {
                            'filter': [
                                'lowercase',
                                'synonym',
                                'stemmer_russian',
                                'unique'
                            ],
                            'char_filter': [
                                'rus_en_key'
                            ],
                            'type': 'custom',
                            'tokenizer': 'standard'
                        },
                        'rus_en_key_analyzer_search': {
                            'filter': [
                                'lowercase',
                            ],
                            'char_filter': [
                                'rus_en_key'
                            ],
                            'type': 'custom',
                            'tokenizer': 'standard'
                        },
                        'latin_to_russian': {
                            'filter': [
                                'lowercase',
                                'latin_to_russian'
                            ],
                            'tokenizer': 'standard'
                        },
                        'autocomplete_analyzer': {
                            'filter': [
                                'lowercase'
                            ],
                            'tokenizer': 'autocomplete'
                        },
                        'autocomplete_analyzer_cyrillic': {
                            'filter': [
                                'lowercase',
                                'latin_to_russian'
                            ],
                            'tokenizer': 'autocomplete'
                        }
                    },
                    'tokenizer': {
                        'autocomplete': {
                            'token_chars': [
                                'letter',
                                'digit'
                            ],
                            'min_gram': '3',
                            'type': 'edge_ngram',
                            'max_gram': '60'
                        }
                    }
                }
            }
        }
        self.mappings = {
            self.index_products: {
                'properties': {
                    'brand': {
                        'type': 'text',
                        'fields': {
                            'cyrillic': {
                                'type': 'text',
                                'analyzer': 'latin_to_russian'
                            },
                            'cyrillic_ngram': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer_cyrillic'
                            }
                        }
                    },
                    'category': {
                        'type': 'text',
                        'fields': {
                            'search': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer',
                                'search_analyzer': 'default'
                            }
                        }
                    },
                    'code': {
                        'type': 'keyword'
                    },
                    'stars': {
                        'type': 'float'
                    },
                    'title': {
                        'type': 'text',
                        'fields': {
                            'cyrillic': {
                                'type': 'text',
                                'analyzer': 'latin_to_russian'
                            },
                            'unique': {
                                'type': 'text'
                            },
                            'cyrillic_ngram': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer_cyrillic',
                                'search_analyzer': 'default'
                            }
                        }
                    },
                    'xml': {
                        'type': 'keyword'
                    }
                }
            },
            self.index_brands: {
                'properties': {
                    'code': {
                        'type': 'keyword'
                    },
                    'title': {
                        'type': 'text',
                        'fields': {
                            'cyrillic': {
                                'type': 'text',
                                'analyzer': 'latin_to_russian'
                            },
                            'search': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer',
                                'search_analyzer': 'default'
                            },
                            'cyrillic_ngram': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer_cyrillic',
                                'search_analyzer': 'default'
                            }
                        }
                    },
                    'xml': {
                        'type': 'keyword'
                    }
                }
            },
            self.index_categories: {
                'properties': {
                    'code': {
                        'type': 'keyword'
                    },
                    'title': {
                        'type': 'text',
                        'fields': {
                            'cyrillic': {
                                'type': 'text',
                                'analyzer': 'latin_to_russian'
                            },
                            'cyrillic_ngram': {
                                'type': 'text',
                                'analyzer': 'autocomplete_analyzer',
                                'search_analyzer': 'default'
                            }
                        }
                    },
                    'xml': {
                        'type': 'keyword'
                    }
                }
            },
        }

    def search(self, index_name: str, body: dict):
        result = self.es_client.search(index=index_name, body=body)
        max_score = result['hits']['max_score']
        total = result['hits']['total']
        items = result['hits']['hits']
        aggregations = []
        if 'aggregations' in result:
            aggregations = result['aggregations']

        return {
            'max_score': max_score,
            'total': total,
            'items': items,
            'aggregations': aggregations
        }

    def get_indexes(self):
        return [
            self.index_products,
            self.index_brands,
            self.index_categories
        ]

    def create_index(self, index_name: str, dict_csv_data: list):
        try:
            self.es_client.indices.delete(index=index_name)
        except Exception as e:
            print(e)

        index_data = {
            'settings': self.settings,
            'mappings': self.mappings[index_name]
        }

        self.es_client.indices.create(index=index_name, body=index_data)

        csv_client = CsvUtil()

        items = csv_client.csv_reader_to_dict(index_name, dict_csv_data)

        counter = 0
        for item in items:
            item_id = item['id']
            del item['id']
            self.es_client.index(index=index_name, id=item_id, body=item)
            counter += 1
            print(counter)


