from service.es import ES


class Search:
    def __init__(self, params: dict):
        self.client = ES()
        self.text = params['text']
        self.from_search = params['from']
        if params['count']:
            self.count = params['count']
        else:
            self.count = 10

    def get_presearch(self):
        is_false_keyboard = self.check_keyboard_layout('example_items')

        body_universal = {
            'size': self.count,
            'query': {
                'multi_match': {
                    'query': self.text,
                    'fields': [
                        'title',
                        'title.search',
                        'title.cyrillic_ngram',
                    ],
                    'fuzziness': 1,
                    'prefix_length': 5
                }
            },
            'highlight': {
                'pre_tags': '<b>',
                'post_tags': '</b>',
                'fields': {
                    'title.cyrillic_ngram': {}
                },
                'require_field_match': 'false',
            }
        }

        body_products = {
            'size': self.count,
            'query': {
                'function_score': {
                    'query': {
                        'multi_match': {
                            'query': self.text,
                            'type': 'most_fields',
                            'fields': [
                                'category^2',
                                'brand^2',
                                'brand.cyrillic^2',
                                'title.cyrillic_ngram',
                                'title.unique',
                                'xml',
                                '_id'
                            ]
                        }
                    },
                    'field_value_factor': {
                        'field': 'stars',
                        'factor': 5,
                        'modifier': 'log1p'
                    }
                }
            },
            'highlight': {
                'pre_tags': '<b>',
                'post_tags': '</b>',
                'fields': {
                    'title.cyrillic_ngram': {}
                },
                'require_field_match': 'false',
            }
        }

        if is_false_keyboard:
            del body_universal['highlight']
            del body_products['highlight']
            body_universal['query']['multi_match']['analyzer'] = 'rus_en_key_analyzer'
            body_products['query']['function_score']['query']['multi_match']['analyzer'] = 'rus_en_key_analyzer'

        products = self.client.search('example_items', body_products)
        brands = self.client.search('example_brands', body_universal)
        categories = self.client.search('example_categories', body_universal)

        return {
            'products': products['items'],
            'brands': brands['items'],
            'categories': categories['items']
        }

    def get_full_search(self):
        is_false_keyboard = self.check_keyboard_layout('example_items')

        body = {
            'size': 1,
            'from': self.from_search,
            'query': {
                'function_score': {
                    'query': {
                        'multi_match': {
                            'query': self.text,
                            'type': 'most_fields',
                            'fields': [
                                'category^2',
                                'brand.cyrillic^2',
                                'brand^2',
                                'title.cyrillic_ngram',
                                'title.unique',
                                'xml',
                                '_id'
                            ]
                        }
                    },
                    'field_value_factor': {
                        'field': 'stars',
                        'factor': 5,
                        'modifier': 'log1p'
                    }
                }
            }
        }

        if is_false_keyboard:
            body['query']['function_score']['query']['multi_match']['analyzer'] = 'rus_en_key_analyzer'

        current_score = self.client.search('example_items', body)['max_score'] * 0.7
        body['min_score'] = current_score
        body['size'] = self.count

        products = self.client.search('example_items', body)

        return {
            'products': products['items'],
            'total': products['total'],
        }

    def check_keyboard_layout(self, index: str):
        body = {
            'size': 0,
            'aggs': {
                'keyboard': {
                    'filters': {
                        'filters': {
                            'eng': {
                                'match': {
                                    'title': {
                                        'query': self.text
                                    }
                                }
                            },
                            'ru': {
                                'match': {
                                    'title': {
                                        'query': self.text,
                                        'analyzer': 'rus_en_key_analyzer'
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        result = self.client.search(index, body)
        return result['aggregations']['keyboard']['buckets']['eng']['doc_count'] \
               < result['aggregations']['keyboard']['buckets']['ru']['doc_count']
