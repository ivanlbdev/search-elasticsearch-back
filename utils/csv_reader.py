import csv


class CsvUtil:
    def __init__(self, base_path: str = '../elasticsearch/', delimiter: str = ','):
        self.base_path = base_path
        self.delimiter = delimiter

    def csv_reader_to_dict(self, file_name: str, dict_data_list: list):
        result = []

        with open(f'{self.base_path}{file_name}.csv', encoding='utf-8', newline='') as f:
            data_csv = csv.DictReader(f, delimiter=self.delimiter)
            for row in data_csv:
                item = {}
                for prop in dict_data_list:
                    item[prop['es_name']] = row[prop['csv_name']]

                result.append(item)

        return result

