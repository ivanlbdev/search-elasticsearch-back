from service.es import ES


def reindex():
    es_client = ES()
    list_indexes = es_client.get_indexes()
    list_data_map = {
        'example_items': [
            {'es_name': 'id', 'csv_name': 'id'},
            {'es_name': 'title', 'csv_name': 'title'},
            {'es_name': 'code', 'csv_name': 'alias'},
            {'es_name': 'xml', 'csv_name': 'xml_id'},
            {'es_name': 'stars', 'csv_name': 'score'},
            {'es_name': 'brand', 'csv_name': 'brand'},
            {'es_name': 'category', 'csv_name': 'category'}
        ],
        'example_brands': [
            {'es_name': 'id', 'csv_name': 'id'},
            {'es_name': 'title', 'csv_name': 'title'},
            {'es_name': 'code', 'csv_name': 'alias'},
            {'es_name': 'xml', 'csv_name': 'xml_id'}
        ],
        'example_categories': [
            {'es_name': 'id', 'csv_name': 'id'},
            {'es_name': 'title', 'csv_name': 'title'},
            {'es_name': 'code', 'csv_name': 'alias'},
            {'es_name': 'xml', 'csv_name': 'xml_id'}
        ]
    }

    for item in list_indexes:
        es_client.create_index(item, list_data_map[item])


if __name__ == '__main__':
    reindex()
