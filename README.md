# Установка backend

## Сборка и запуск

Устанока виртуального огружения:
```shell
$ cd <папка_с_проектом> python -m venv venv
$ source venv/bin/activate
```

Установка зависисмостей:
```shell
$ cd <папка_с_проектом> pip install
```

Запуск сервера:
```shell
$ cd <папка_с_проектом> python app.py
```
